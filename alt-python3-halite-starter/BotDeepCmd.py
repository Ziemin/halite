import random
import numpy as np
import os
import sys
import sys
import logging
import time
from library.bots import TfMultiCellBot, MultiCellInputProvider
import library.hlt as hlt

logging.basicConfig(filename='MyBot.log', level=logging.DEBUG)

model_file = sys.argv[1]

with open(os.devnull, 'w') as sys.stderr:
  from library.models import model_multi_conv_10
  model = model_multi_conv_10(False)

  myID, game_map = hlt.get_init()

  bot = TfMultiCellBot(model, model_file, game_map,
      MultiCellInputProvider(myID, game_map, padding=9), on_gpu=False)

hlt.send_init(os.path.basename(model_file))

iters = 0
while True:
  with open(os.devnull, 'w') as sys.stderr:
    moves = bot.next_moves(stochastic=False)

  hlt.send_frame(moves)
  iters += 1
