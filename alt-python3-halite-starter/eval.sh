#!/bin/bash - 
set -o nounset                              # Treat unset variables as an error
set -e

c=1
while [ $c -le 10 ]; do
	echo "-------- Round: " $c "----------------"
  ./halite -d "35 35" "python MyBot.py 2> /dev/null" "python backup/bot69/MyBot.py 2> /dev/null" "python backup/bot49/MyBot.py 2> /dev/null" | grep  -v 'Turn'
  let c++
done;

paplay /usr/share/sounds/Oxygen-Im-Phone-Ring.ogg
