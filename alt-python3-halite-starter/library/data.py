import pickle
import pandas as pd
import numpy as np
from functools import partial
from scipy import ndimage
import logging
import argparse
import os
import gc
import logging

def random_shuffle(X, y):
  p = np.random.permutation(len(y))
  return (X[p], y[p])

def get_next_batch(X, y, batch_size, step):
  offset = (step * batch_size) % (y.shape[0] - batch_size)
  batch_data = X[offset:(offset + batch_size), :, :, :]
  batch_labels = y[offset:(offset + batch_size), :]
  return (batch_data, batch_labels)

def random_rotate(X, y):
  def rotate_x_elem(el, rot):
    new_el = np.copy(el)
    for i in range(4):
      new_el[:, :, i] = np.rot90(el[:, :, i], rot)
    return new_el

  rotations = np.random.randint(0, 4, len(X))
  X_rotated = np.array([rotate_x_elem(el, rot)
      for (el, rot) in zip(X, rotations)])
  y_rotated = np.array([np.rot90(el, rot)
      for (el, rot) in zip(y, rotations)])
  return (X_rotated, y_rotated)

def random_flip(X, y):
  def flip_one(el, code):
    if code == 0:
      return el
    elif code == 1:
      return np.fliplr(el)
    elif code == 2:
      return np.flipud(el)
    else:
      return np.fliplr(np.flipud(el))

  def flip_x_elem(el, code):
    new_el = np.copy(el)
    for i in range(4):
      new_el[:, :, i] = flip_one(el[:, :, i], code)
    return new_el

  flips = np.random.randint(0, 4, len(X))
  X_flipped = np.array([flip_x_elem(el, code)
      for (el, code) in zip(X, flips)])
  y_flipped = np.array([flip_one(el, code)
      for (el, code) in zip(y, flips)])
  return (X_flipped, y_flipped)

def get_winner_number(frames):
  last_frame = frames[-1]
  owners = last_frame[:,:,[0]].reshape(-1)
  owners = owners[owners != 0]
  owners = pd.DataFrame({'owner': owners})
  owners['c'] = 1
  counts = owners.groupby('owner').sum()
  if counts.c.empty:
    return 1
  return counts.c.argmax()

def get_rows_where_winner(replays, username):
  """ returns the rows where given @username was a winner """
  total = len(replays)
  user_ids = replays.player_names.apply(
      lambda names: [x.split()[0] for x in names].index(username) + 1)

  replays = replays[user_ids == replays.winner]
  logging.info("Rows total: {0}, Rows where {1} is a winner {2}".format(
    total, username, len(replays)))
  return replays

def remove_empty_frames(replays):
  def remove_empty(row):
    frames = row.frames
    me = row.me
    moves = row.moves
    owners = frames[:, :, :, 0]
    my_cells = np.where(owners == me, 1, 0)
    sums = np.sum(my_cells, axis=(1, 2))
    frames = frames[sums > 0]
    moves = moves[:len(frames)-1]
    num_frames = len(frames)
    assert(len(moves) == len(frames)-1)
    return pd.Series(
        [me, frames, moves, num_frames],
        index=['me', 'frames', 'moves', 'num_frames'])

  new_columns  = replays[['me', 'frames', 'moves', 'num_frames']].apply(remove_empty, axis=1)
  replays.frames = new_columns.frames
  replays.moves = new_columns.moves
  replays.num_frames = new_columns.num_frames

def preprocess_replays_df(df, username, min_game_length):
  # Remove too short games
  df = df[df.frames.apply(len) >= min_game_length]

  # Add winner column
  winner_column = df.frames.apply(get_winner_number)
  df['winner'] = winner_column
  me_column = df.player_names.apply(
      lambda names: [x.split()[0] for x in names].index(username) + 1)
  df['me'] = me_column

  return df

def user_games_to_df(games, username):
  replay_names = np.array([game[0] for game in games])
  version = np.array([game[1]['version'] for game in games])
  height = np.array([game[1]['height'] for game in games])
  width = np.array([game[1]['width'] for game in games])
  num_players = np.array([game[1]['num_players'] for game in games])
  num_frames = np.array([game[1]['num_frames'] for game in games])
  player_names = np.array([np.array(game[1]['player_names']) for game in games])
  moves = np.array([np.array(game[1]['moves']) for game in games])
  frames = np.array([np.array(game[1]['frames']) for game in games])
  productions = np.array([np.array(game[1]['productions']) for game in games])

  df = pd.DataFrame({
      'version': version,
      'height': height,
      'width': width,
      'num_players': num_players,
      'num_frames': num_frames,
      'player_names': player_names,
      'moves': moves,
      'frames': frames,
      'productions': productions
      },
      index=replay_names)

  return preprocess_replays_df(df, username, 10)

def get_productions(replays_df):
  """ copies productions array #frames times """
  def copy_prod(row):
    times = row.number
    multiplied = np.tile(row.productions, (times, 1, 1)).astype('float32')
    return pd.Series([times, multiplied], index=['number', 'productions'])

  frame_counts = replays_df.num_frames
  tmp_df = pd.DataFrame(
          {'number': frame_counts, 'productions': replays_df.productions})
  productions_vals = tmp_df.apply(copy_prod, axis=1).productions.values

  return productions_vals

def get_strengths(replays_df):
  def extract_strength(frames):
    return frames[:, :, :, [1]].reshape(
            frames.shape[0], frames.shape[1], frames.shape[2]).astype('float32')

  strengths = replays_df.frames.apply(extract_strength).values
  return strengths

def get_owners(replays_df):
  def set_owners(row, enemy=False):
    frames = row.frames
    me = row.me
    owners = frames[:, :, :, [0]].reshape(
            frames.shape[0], frames.shape[1], frames.shape[2])
    new_owners = np.copy(owners)
    if enemy:
      new_owners[owners != me] = 1
      new_owners[owners == me] = 0
      new_owners[owners == 0] = 0
    else:
      new_owners[owners == me] = 1
      new_owners[owners != me] = 0

    return pd.Series([me, new_owners.astype('float32')], index=['me', 'owners'])

  me = replays_df[['me', 'frames']].apply(partial(set_owners, enemy=False), axis=1).owners.values
  enemy = replays_df[['me', 'frames']].apply(partial(set_owners, enemy=True), axis=1).owners.values
  return (me, enemy)

def get_moves(replays_df):
  def from_game(moves):
      new_moves = np.zeros(
              (moves.shape[0], moves.shape[1], moves.shape[2], 5),
              dtype=int)
      new_moves[np.where(moves == 0)] = [1,0,0,0,0]
      new_moves[np.where(moves == 1)] = [0,1,0,0,0]
      new_moves[np.where(moves == 2)] = [0,0,1,0,0]
      new_moves[np.where(moves == 3)] = [0,0,0,1,0]
      new_moves[np.where(moves == 4)] = [0,0,0,0,1]
      return new_moves.astype('float32')

  moves = replays_df.moves.values
  new_moves = np.array([from_game(x) for x in moves])
  return new_moves

def drop_last_transform(names, data):
  """ Removes last frame from every game """
  for name in names:
      data[name] = np.array([np.delete(x, (-1), axis=0) for x in data[name]])

  return data

def take_first_frames_transform(fraction, data):
  for name, arr in data.items():
      data[name] = np.array(
              [np.delete(x, np.s_[int(fraction * x.shape[0]):], axis=0)
               for x in arr])
  return data

def previous_frame_transform(keys, data):
  def get_prevs(game):
    prev_frames = game[:-1]
    return np.concatenate([game[0:1], prev_frames])

  for name in keys:
      data['prev_' + name] = np.array(
              [get_prevs(game) for game in data[name]])
  return data

def normalize_transform(divs, data):
  for name, div in divs.items():
    data[name] = data[name] / div
  return data

def rotation_transform(data):
  """ Takes dataset and adds 3 rotated versions of it """
  def rotate_game(k, game):
      return np.array([np.rot90(x,k) for x in game])
  def rotate(k, arr):
      fun = np.vectorize(lambda x: rotate_game(k, x), otypes=[object])
      return fun(arr)

  for (name, result) in data.items():
      r1, r2, r3 = rotate(1, result), rotate(2, result), rotate(3, result)
      data[name] = np.concatenate([result, r1, r2, r3])

  return data

def rotate_once_transform(k, data):
  def rotate_game(k, game):
    return np.array([np.rot90(x,k) for x in game])
  def rotate(k, arr):
    return np.array([rotate_game(k, x) for x in arr])

  for (name, result) in data.items():
      data[name] = rotate(k, result)

  return data

def break_frame(frame, pad, indices):
  results = []
  for i in range(len(indices[0])):
      h = indices[0][i]
      w = indices[1][i]
      sub_frame = np.take(frame, range(h - pad, h + pad + 1), axis=0, mode='wrap')
      sub_frame = np.take(sub_frame, range(w - pad, w + pad + 1), axis=1, mode='wrap')
      results.append(sub_frame)

  rs = np.array(results)
  return rs

def unroll_transform(padding, data):
  owners = data['me']
  def pad(game, owners):
      results = [break_frame(frame, padding, np.where(own == 1))
                  for (frame, own) in zip(game, owners)]
      results = np.concatenate(results)
      return results

  fun = np.vectorize(lambda x: pad(x[0], x[1]), otypes=[object])
  for name in ['strengths', 'productions', 'me', 'enemy']:
      data[name] = np.concatenate([pad(game, own)
          for (game, own) in zip (data[name], owners)])

  moves = [game_moves[np.where(own==1)] for (game_moves, own) in zip(data['moves'], owners)]
  data['moves'] = np.concatenate(moves)
  assert(data['strengths'].shape == data['productions'].shape)
  assert(data['productions'].shape == data['me'].shape)
  assert(data['productions'].shape[0] == data['moves'].shape[0])

  return data

def pad(game, height, width, padding):
  h, w = game.shape[1], game.shape[2]
  h_left = height - h
  w_left = width - w
  h_u = padding + h_left // 2
  h_d = padding + (h_left - h_left // 2)
  w_l = padding + w_left // 2
  w_r = padding + (w_left - w_left // 2)
  pad_width = ((h_u, h_d), (w_l, w_r))
  while len(pad_width) < len(game.shape[:-1]):
    pad_width += ((0,0),)

  new_game = np.array([np.pad(x, pad_width, mode='wrap') for x in game])
  desired_shape = list(game.shape)
  desired_shape[1] = height + 2*padding
  desired_shape[2] = width + 2*padding
  assert(new_game.shape == tuple(desired_shape))
  return new_game

def unwrap_tranform(names, height, width, padding, data):

  for name in names:
    data[name] = np.concatenate([pad(game, height, width, padding) for game in data[name] if game.any()])

  return data

def avg_pool(frame, filtr, window_size):
  h, w = frame.shape
  to_cut = window_size // 2
  convolved = ndimage.convolve(frame, filtr, mode='constant')

  return convolved[to_cut:h-to_cut, to_cut:w-to_cut]

def avg_pooling_transform(window_size, data):

  batch = 2048
  import tensorflow as tf
  from math import ceil
  for name, value in data.items():
    value = np.expand_dims(value, len(value.shape))
    res = []
    with tf.Graph().as_default():
      X = tf.placeholder(tf.float32, [None, 100, 100, 1])
      X_avg = tf.nn.avg_pool(X, [1, window_size, window_size, 1], [1,1,1,1], padding='VALID')
      with tf.Session() as sess:
        steps = ceil(len(value) / batch)
        for step in range(steps):
          begin = step * batch
          end = min((step+1) * batch, len(value))
          next_batch = value[begin:end]
          X_new = sess.run(X_avg, feed_dict={X: next_batch})
          X_new = X_new.reshape(X_new.shape[:-1])
          res.append(X_new)

      data[name] = np.concatenate(res)

  return data

def merge_transform(names, data, axis=3):
  to_concat = [np.expand_dims(data[name], axis=axis) for name in names]
  concatenated = np.concatenate(to_concat, axis=axis)

  # rewriting other keys
  result = { 'data': concatenated }
  for name, value in data.items():
    if name not in names:
      result[name] = value

  return result

def print_memory_stats(data):
  def get_size_in_gb(arr):
    size = 0
    for x in arr:
      size += x.nbytes
    return size / (1 << 30)

  for name, arr in data.items():
    print("{0}: {1:.2f} GB".format(name, get_size_in_gb(arr)))

def chain_functions(*functions):
  def composition(argument):
    for f in functions:
      logging.info(f)
      argument = f(argument)
      gc.collect()
      # print_memory_stats(argument)
    return argument

  return composition

def create_dataset(replays, username, max_games=60):
  replays = get_rows_where_winner(replays, username)
  max_games = min(max_games, len(replays))
  if max_games is not None:
    replays = replays[:max_games]
  prods = get_productions(replays)
  strengths = get_strengths(replays)
  me, enemy = get_owners(replays)
  moves = get_moves(replays)

  data = { 'productions': prods
         , 'strengths': strengths
         , 'me': me
         , 'enemy': enemy
         , 'moves': moves }

  trensformation = chain_functions(
          partial(drop_last_transform, ['productions', 'strengths', 'me', 'enemy']),
          partial(take_first_frames_transform, 0.80),
          partial(normalize_transform, {'productions': 20, 'strengths': 255}),
          partial(unroll_transform, 9),
          partial(merge_transform, ['productions', 'strengths', 'me', 'enemy']))

  dataset = trensformation(data)
  return { 'data': dataset['data'], 'target': dataset['moves'] }

def create_dataset_multi(replays, username, rotation, visibility_range=9):
  # replays = get_rows_where_winner(replays, username)
  remove_empty_frames(replays)
  prods = get_productions(replays)
  strengths = get_strengths(replays)
  me, enemy = get_owners(replays)
  moves = get_moves(replays)

  # enemy_strengths = [np.where(own == -1, stren, 0) for (own, stren) in zip(owners, strengths)]

  data = { 'productions': prods
         , 'strengths': strengths
         , 'me': me
         , 'enemy': enemy
         , 'moves': moves }

  trensformation = chain_functions(
          partial(drop_last_transform, ['productions', 'strengths', 'me', 'enemy']),
          # partial(take_first_frames_transform, 0.80),
          partial(rotate_once_transform, rotation),
          partial(normalize_transform, {'productions': 20, 'strengths': 255}),
          # partial(previous_frame_transform, ['strengths', 'me', 'enemy']),
          partial(unwrap_tranform,
            ['productions', 'strengths', 'me', 'enemy'],
              # 'prev_strengths', 'prev_me', 'prev_enemy'],
            50, 50, visibility_range),
          partial(unwrap_tranform,
            ['moves'],
            50, 50, 0),
          partial(merge_transform, [
            'productions', 'strengths', 'me', 'enemy']))
            # 'prev_strengths', 'prev_me', 'prev_enemy']))

  dataset_1 = trensformation(data)

  # data = {  'enemy_strengths': enemy_strengths }

  # window_size = 51 - 2*visibility_range
  # trensformation = chain_functions(
          # partial(drop_last_transform, data.keys()),
          # partial(take_first_frames_transform, 0.60),
          # partial(rotate_once_transform, rotation),
          # partial(normalize_transform,
            # { 'enemy_strengths': 255 }),
          # partial(unwrap_tranform, data.keys(), 50, 50, 25),
          # partial(avg_pooling_transform, window_size),
          # partial(merge_transform, data.keys()))

  dataset_2 = None

  # resulting_data = np.concatenate(
      # [dataset_1['data'], dataset_2['data']], axis=3)

  # X = resulting_data
  X = dataset_1['data']
  y = dataset_1['moves']

  dataset_1 = dataset_2 = data = enemy_strengths = None
  replays = prods = strengths = owners = moves = None
  transformation = None
  gc.collect()

  X, y = random_shuffle(X, y)

  return { 'data': X
         , 'target': y }


def save_to_npy(datasets, directory, prefix):
    X = datasets['data']
    y = datasets['target']

    target_X = os.path.join(directory, prefix + "_X.npy")
    target_y = os.path.join(directory, prefix + "_y.npy")

    np.save(target_X, X)
    np.save(target_y, y)

def load_from_npy(directory, prefix):
    X_path = os.path.join(directory, prefix + "_X.npy")
    y_path = os.path.join(directory, prefix + "_y.npy")

    return (np.load(X_path), np.load(y_path))
