import tensorflow as tf

def variable_summaries(var, name):
  """Attach a lot of summaries to a Tensor."""
  with tf.name_scope('summaries'):
    mean = tf.reduce_mean(var)
    tf.scalar_summary('mean/' + name, mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.scalar_summary('stddev/' + name, stddev)
    tf.scalar_summary('max/' + name, tf.reduce_max(var))
    tf.scalar_summary('min/' + name, tf.reduce_min(var))
    tf.histogram_summary(name, var)

def get_accuracy(logits, y_):
  with tf.name_scope('accuracy'):
    with tf.name_scope('correct_prediction'):
      correct_predictions = tf.equal(tf.argmax(logits, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))
    tf.scalar_summary('accuracy', accuracy)
  return accuracy

def get_still_accuracy(logits, y_):
  with tf.name_scope('still_accuracy'):
    with tf.name_scope('correct_prediction'):
      stills_ = tf.equal(tf.argmax(y_, 1), 0, name="stills_")
      stills = tf.equal(tf.argmax(logits, 1), 0, name="stills")
      correct_predictions = tf.equal(stills_, stills)
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))
    tf.scalar_summary('still_accuracy', accuracy)
    return accuracy

def get_moves_accuracy(logits, y_):
  with tf.name_scope('moves_accuracy'):
    with tf.name_scope('correct_prediction'):
      indices = tf.reshape(
          tf.where(tf.not_equal(tf.argmax(y_, 1), 0, name="stills_")),
          [-1])
      moves_ = tf.gather(y_, indices)
      moves = tf.gather(logits, indices)
      correct_predictions = tf.equal(tf.argmax(moves, 1), tf.argmax(moves_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))
    tf.scalar_summary('moves_accuracy', accuracy)

    for i in range(1, 5):
      indices = tf.reshape(
          tf.where(tf.equal(tf.argmax(y_, 1), i, name="stills_")),
          [-1])
      moves_i_ = tf.gather(y_, indices)
      moves_i = tf.gather(logits, indices)
      correct_predictions = tf.equal(tf.argmax(moves_i, 1), tf.argmax(moves_i_, 1))
      accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))
      tf.scalar_summary('moves_accuracy_'+str(i), accuracy)
    return accuracy

def print_stats(name, step, loss, acc, still_acc, moves_acc):
  print("{2} loss at step {0}: {1:.3f}".format(
      step, loss, name))
  print("{2} accuracy at step {0}: {1:.2f}%".format(
      step, 100*acc, name))
  print("{2} still accuracy at step {0}: {1:.2f}%".format(
      step, 100*still_acc, name))
  print("{2} moves accuracy at step {0}: {1:.2f}%".format(
      step, 100*moves_acc, name))

