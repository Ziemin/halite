import numpy as np
import tensorflow as tf
import library.stats as stats
from library.data import get_next_batch, random_flip
import logging

def leakyReLU(x, alpha=0.01, name='leakyReLU'):
  return tf.maximum(alpha * x, x, name=name)

def batch_normalization(preactivate, offset, scale, epsi=1e-3, decay=0.999, is_training=False):

  pop_mean = tf.Variable(tf.zeros([preactivate.get_shape()[-1]]), trainable=False)
  pop_var = tf.Variable(tf.ones([preactivate.get_shape()[-1]]), trainable=False)

  if is_training:
    batch_mean, batch_var = tf.nn.moments(
        preactivate, list(range(len(preactivate.get_shape().as_list()) - 1)))
    train_mean = tf.assign(pop_mean,
                           pop_mean * decay + batch_mean * (1 - decay))
    train_var = tf.assign(pop_var,
                          pop_var * decay + batch_var * (1 - decay))
    with tf.control_dependencies([train_mean, train_var]):
        return tf.nn.batch_normalization(preactivate,
            batch_mean, batch_var, offset, scale, epsi)
  else:
    return tf.nn.batch_normalization(preactivate,
        pop_mean, pop_var, offset, scale, epsi)

class Conv2dLayer:
  def __init__(self, name, input_tensor, shape=None,
      act=tf.nn.relu, padding="VALID", use_batch_norm=True,
      dropout=None, weights=None, biases=None, is_training=False):

    self.name = name
    with tf.name_scope(name):
      with tf.name_scope('weights'):
        if weights is None:
          # Xavier initializaiton N(0,1)/sqrt(fan_in/2)
          self.weights = tf.Variable(
              tf.truncated_normal(shape) / np.sqrt(shape[0] * shape[1] * shape[2] / 2))
        else:
          self.weights = weights
      with tf.name_scope('biases'):
        if biases is None:
          self.biases = tf.Variable(tf.zeros([shape[-1]]))
        else:
          self.biases = biases
      with tf.name_scope('conv2d'):
        preactivate = tf.nn.conv2d(
            input_tensor, self.weights, [1,1,1,1], padding=padding)
      if use_batch_norm:
        epsi = 1e-3
        offset = self.biases
        scale = tf.Variable(tf.ones([shape[-1]]))
        preactivate = batch_normalization(
            preactivate, offset, scale, epsi, is_training=is_training)
      else:
        preactivate = tf.nn.bias_add(preactivate, self.biases)
      if act is not None:
        self.output = act(preactivate, name='activation')
      else:
        self.output = preactivate

      if dropout is not None:
        self.output = tf.nn.dropout(self.output, dropout)


class FullLayer:
  def __init__(self, name, input_tensor, size=None, act=tf.nn.relu, use_batch_norm=True,
      weights=None, biases=None, is_training=False):
    self.name = name
    input_size = input_tensor.get_shape().as_list()[-1]
    with tf.name_scope(name):
      with tf.name_scope('weights'):
        if weights is None:
          self.weights = tf.Variable(
              tf.truncated_normal([input_size, size]) / np.sqrt(input_size))
        else:
          self.weights = weights
      with tf.name_scope('biases'):
        if biases is None:
          self.biases = tf.Variable(tf.truncated_normal([size], stddev=0.1))
        else:
          self.biases = biases
      with tf.name_scope('full'):
        preactivate = tf.matmul(input_tensor, self.weights)
      if use_batch_norm:
        epsi = 1e-3
        offset = self.biases
        scale = tf.Variable(tf.ones([shape[-1]]))
        preactivate = batch_normalization(
            preactivate, offset, scale, epsi, is_training=is_training)
      else:
        preactivate = preactivate + self.biases
      if act is not None:
        self.output = act(preactivate, name='activation')
      else:
        self.output = preactivate

class NNModel:

  def __init__(self, name, graph, X, prediction, samples, layers, visible=9):
    self.name = name
    self.graph = graph
    self.layers = layers
    self.input = X
    self.output = self.layers[-1].output
    self.prediction = prediction
    self.samples = samples
    self.visible = visible
    with self.graph.as_default():
      self.saver = tf.train.Saver(max_to_keep=1000)

  def predict(self, session, data, stochastic=False, batch_size=512):
    with self.graph.as_default():
      with session.as_default():
        outputs = []
        batch_size = min(batch_size, len(data))
        steps = round(len(data)/batch_size)

        for i in range(steps):
          outputs.append(
              session.run(self.samples if stochastic else self.prediction,
              feed_dict={self.input: data[i*batch_size:(i+1)*batch_size]}))

        return np.concatenate(outputs)

  def save(self, session, target_file, **args):
    with self.graph.as_default():
      self.saver.save(session, target_file, **args)

  def restore(self, session, target_file):
    with self.graph.as_default():
      self.saver.restore(session, target_file)

class Trainer:

  def __init__(self, model, learning_rate=0.001, batch_size=512, iters=20000):
    self.model = model
    self.learning_rate = learning_rate
    self.batch_size = batch_size
    self.iters = iters
    self._construct_graph()

  def _construct_graph(self):
    with self.model.graph.as_default():
      self.y = tf.placeholder(tf.float32, [None, 50, 50, 5])
      X = self.model.input
      v = self.model.visible
      # we need to get indices of our cells
      # first, extract one channel from X -> getting shape [?, 50, 50]

      owners = tf.reshape(
          tf.slice(X, [0, v, v, 2], [-1, 50, 50, 1]),
          [-1])
      # select indices where owner == 1
      indices = tf.reshape(
          tf.where(tf.equal(owners, 1)),
          [-1])

      flat_y = tf.reshape(self.y, [-1, 5])

      good_y = tf.gather(tf.reshape(self.y, [-1, 5]), indices)
      stills = tf.slice(good_y, [0, 0], [-1, 1])
      weights = tf.reshape(
          tf.add(tf.mul(stills, -0.75), 1.0),
          [-1])
      logits = tf.gather(tf.reshape(self.model.output, [-1, 5]), indices)

      with tf.name_scope('cross_entropy'):
        self.loss = tf.reduce_mean(tf.mul(weights, tf.nn.softmax_cross_entropy_with_logits(logits, good_y)))
        # self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits, good_y))
        tf.scalar_summary('cross_entropy', self.loss)

      with tf.name_scope('train'):
        self.train_step = tf.train.AdamOptimizer(
                learning_rate=self.learning_rate,
                beta1=0.83
                # epsilon=0.05,
                ).minimize(self.loss)
        # self.train_step = tf.train.RMSPropOptimizer(
                # learning_rate=self.learning_rate,
                # decay=0.8
                # ).minimize(self.loss)
        # self.train_step = tf.train.AdadeltaOptimizer(
                # learning_rate=self.learning_rate
                # ).minimize(self.loss)

      self.stats = {
          'accuracy': stats.get_accuracy(logits, good_y),
          'still_accuracy': stats.get_still_accuracy(logits, good_y),
          'moves_accuracy': stats.get_moves_accuracy(logits, good_y)
          }
      self.merged_summaries = tf.merge_all_summaries()

  def _eval_validation(self, session, X_valid, y_valid, step, test_writer):
    ls = []
    accs = []
    s_accs = []
    m_accs = []

    v_steps = len(X_valid) // self.batch_size
    for i in range(v_steps):
      batch_data, batch_labels = get_next_batch(X_valid, y_valid, self.batch_size, i)
      l, acc, s_acc, m_acc = session.run(
          [ self.loss, self.stats['accuracy']
          , self.stats['still_accuracy']
          , self.stats['moves_accuracy'] ],
          feed_dict={self.model.input: batch_data, self.y: batch_labels})
      ls.append(l)
      accs.append(acc)
      s_accs.append(s_acc)
      m_accs.append(m_acc)

    summary = tf.Summary()
    l = float(np.mean(ls))
    acc = float(np.mean(accs))
    s_acc = float(np.mean(s_accs))
    m_acc = float(np.mean(m_accs))
    summary.value.add(tag='accuracy', simple_value=acc)
    summary.value.add(tag='still_accuracy', simple_value=s_acc)
    summary.value.add(tag='moves_accuracy', simple_value=m_acc)
    summary.value.add(tag='cross_entropy', simple_value=l)

    test_writer.add_summary(summary, step)
    stats.print_stats("Validation", step, l, acc, s_acc, m_acc)
    print("============================")


  def train(self, session, X, y, use_valid=True, save_file=None, global_step=None):
    if use_valid:
      train_size = round(0.95 * len(y))
      X_train = X[:train_size]
      y_train = y[:train_size]
      X_valid = X[train_size:]
      y_valid = y[train_size:]
      # X_valid, y_valid = random_flip(X_valid, y_valid)
    else:
      X_train = X
      y_train = y

    with self.model.graph.as_default():
      with session.as_default():
        train_writer = tf.train.SummaryWriter(
                '/tmp/logs/train', session.graph)
        test_writer = tf.train.SummaryWriter(
                '/tmp/logs/test', session.graph)

        if global_step is None:
          tf.initialize_all_variables().run()
        accuracy = self.stats['accuracy']
        still_accuracy = self.stats['still_accuracy']
        moves_accuracy = self.stats['moves_accuracy']

        global_step = global_step if global_step is not None else 0
        for step in range(global_step, self.iters):
          batch_data, batch_labels = get_next_batch(X_train, y_train, self.batch_size, step)
          # batch_data, batch_labels = random_flip(batch_data, batch_labels)
          feed_dict = { self.model.input: batch_data, self.y: batch_labels }
          run_metadata = tf.RunMetadata()

          _, l, acc, s_acc, m_acc, summary = session.run(
                [self.train_step, self.loss, accuracy,
                  still_accuracy, moves_accuracy, self.merged_summaries],
                feed_dict=feed_dict, run_metadata=run_metadata)

          train_writer.add_run_metadata(run_metadata, 'step%03d' % step)
          train_writer.add_summary(summary, step)

          if (step % 50 == 0):
            stats.print_stats("Minibatch", step, l, acc, s_acc, m_acc)
            print("----------------------------")
            if use_valid:
              self._eval_validation(session, X_valid, y_valid, step, test_writer)

          if (step % 100 == 0):
            if save_file is not None:
              model_file = save_file + '_' + str(step)
              self.model.save(session, model_file)

        train_writer.close()
        test_writer.close()
