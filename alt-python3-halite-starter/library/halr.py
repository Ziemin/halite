#!/usr/bin/python

import argparse
import logging
import pandas as pd
import numpy as np
import tensorflow as tf
import os
import gc

import library.fetching as fetching
import library.data as data
import library.models as models
import library.nets as nets

def fetch(args):
  username = args.username
  target = os.path.join(args.dir, username + "_replays.pkl")

  user = fetching.get_user(args.username)
  replays = fetching.download_user_replays(user)
  df = data.user_games_to_df(replays, username)
  logging.info("Saving data frame to file {}".format(target))
  df.to_pickle(target)

def to_npy(args):
  in_file = args.file
  directory = args.dir
  prefix = args.prefix
  df = pd.read_pickle(in_file)
  if args.type == 2:
    dataset = data.create_dataset_multi(df, args.username, rotation=0)
    data.save_to_npy(dataset, directory, prefix)
  else:
    dataset = data.create_dataset(df, args.username)
    X = dataset['data']
    y = dataset['target']
    np.save(os.path.join(directory, prefix + "_X.npy"), X)
    np.save(os.path.join(directory, prefix + "_y.npy"), y)

def train(args):
  tf.logging.set_verbosity(tf.logging.INFO)

  logging.info("Model graph creation")
  model = models.model_multi_conv_10(is_training=True)
  model_file = os.path.join(args.model_dir, model.name)

  config = tf.ConfigProto()
  config.gpu_options.allow_growth=True
  sess = tf.Session(graph=model.graph, config=config)

  if args.cont_model is not None:
    tf.train.import_meta_graph(args.cont_model + '.meta')
    model.restore(sess, args.cont_model)

  logging.info("Reading data")
  X = np.load(args.X)
  y = np.load(args.y)
  logging.info("---- X shape: {0}, y shape: {1}".format(
      X.shape, y.shape))

  trainer = nets.Trainer(
      model, learning_rate=0.00095, batch_size=210, iters=500000)
  logging.info("Training model: {}".format(model.name))
  global_step = None
  if args.cont_model is not None:
    global_step = int(args.cont_model.split('_')[-1]) + 1
  trainer.train(sess, X, y, True, model_file, global_step=global_step)



parser = argparse.ArgumentParser(description='Halite run environment')

subparsers = parser.add_subparsers()

# ============ FETCH PARSER ================================================

fetch_parser = subparsers.add_parser('fetch', help='Download games')
fetch_parser.add_argument('username', type=str,
        help='Name of the user')
fetch_parser.add_argument('dir', type=str, default='.',
        help='Directory to store games')
fetch_parser.set_defaults(func=fetch)

# ============ DATA PARSER ================================================

data_parser = subparsers.add_parser('npy', help='Data tranformation')
data_parser.add_argument('file', type=str,
        help='Path to pickled dataframe')
data_parser.add_argument('dir', type=str, default='.',
        help='Directory to store the resulting dataset')
data_parser.add_argument('prefix', type=str, default='.',
        help='Prefix of the resulting file')
data_parser.add_argument('username', type=str,
        help='Prefix of the resulting file')
data_parser.add_argument('--type', type=int, default=1,
        help='Type of the dataset')
data_parser.set_defaults(func=to_npy)

# =========== TRAINING PARSER =============================================

train_parser = subparsers.add_parser('train', help='Train model')
train_parser.add_argument('X', type=str, help='Input data file')
train_parser.add_argument('y', type=str, help='Labels file')
train_parser.add_argument('--model_dir', type=str, default='.', help='Directory of serialized model')
train_parser.add_argument('--cont_model', type=str, default=None, help='File to load model from')
train_parser.set_defaults(func=train)


# ========== EXECUTION ===================================================
logging.basicConfig(level=logging.INFO)

args = parser.parse_args()
args.func(args)
