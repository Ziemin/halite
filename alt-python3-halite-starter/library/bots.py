import logging
import os
import sys
import numpy as np
from library.hlt import NORTH, EAST, SOUTH, WEST, STILL, Move, Square
from library.data import *

class CellInputProvider:

    def __init__(self, my_id, game_map, padding):
        self.my_id = my_id
        self.game_map = game_map
        self.padding = padding

    def _break_frame(self, frame, pad, indices):
      results = []
      elements = []
      for i in range(len(indices[0])):
        h = indices[0][i]
        w = indices[1][i]
        sub_frame = np.take(frame, range(h - pad, h + pad + 1), axis=0, mode='wrap')
        sub_frame = np.take(sub_frame, range(w - pad, w + pad + 1), axis=1, mode='wrap')
        results.append(sub_frame)
        elements.append((h, w))

      rs = np.array(results)
      return (rs, elements)

    def _merge(self, productions, strengths, me, enemy):
      strengths = np.expand_dims(strengths, axis=3)
      productions = np.expand_dims(productions, axis=3)
      me = np.expand_dims(me, axis=3)
      enemy = np.expand_dims(enemy, axis=3)
      data = np.concatenate([productions, strengths, me, enemy], axis=3)
      return data

    def next_input(self):
      self.game_map.get_frame()

      contents = self.game_map.contents

      prods = np.array([[s.production / 20 for s in row] for row in contents])
      strengths = np.array([[s.strength / 255 for s in row] for row in contents])
      me = np.array([[1 if s.owner == self.my_id else 0 for s in row] for row in contents])
      enemy = np.array([[1 if s.owner != self.my_id and s.owner != 0 else 0  for s in row] for row in contents])
      indices = np.where(me == 1)

      prods, elements = self._break_frame(prods, self.padding, indices)
      strengths, _ = self._break_frame(strengths, self.padding, indices)
      me, _ = self._break_frame(me, self.padding, indices)
      enemy, _ = self._break_frame(enemy, self.padding, indices)

      data = self._merge(prods, strengths, me, enemy)
      return (data, elements)

class MultiCellInputProvider:

    def __init__(self, my_id, game_map, padding):
        self.my_id = my_id
        self.game_map = game_map
        self.padding = padding

    def next_input(self):
      visibility_range = self.padding
      self.game_map.get_frame()

      contents = self.game_map.contents

      prods = np.array([[[[s.production for s in row] for row in contents]]])
      strengths = np.array([[[[s.strength for s in row] for row in contents]]])
      me = np.array([[[[1 if s.owner == self.my_id else 0 for s in row] for row in contents]]])
      enemy = np.array([[[[1 if s.owner != self.my_id and s.owner != 0 else 0  for s in row] for row in contents]]])

      data = { 'productions': prods
             , 'strengths': strengths
             , 'me': me
             , 'enemy': enemy }

      transformation = chain_functions(
              partial(rotate_once_transform, 0),
              partial(normalize_transform, {'productions': 20, 'strengths': 255}),
              partial(unwrap_tranform,
                ['productions', 'strengths', 'me', 'enemy'],
                50, 50, visibility_range),
              partial(merge_transform, ['productions', 'strengths', 'me', 'enemy']))

      return transformation(data)['data']

class TfBot:

  def __init__(self, model, model_file, game_map,
      input_provider, batch_size=2018, on_gpu=True):

    self.model = model
    self.input_provider = input_provider
    self.game_map = game_map
    self.batch_size = batch_size

    import tensorflow as tf
    if not on_gpu:
      config = tf.ConfigProto(
          intra_op_parallelism_threads=1, inter_op_parallelism_threads=1,
          device_count = {'GPU': 0})
      self.session = tf.Session(config=config, graph=self.model.graph)
    else:
      config = tf.ConfigProto(
          intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
      self.session = tf.Session(graph=self.model.graph)

    with model.graph.as_default():
      model.restore(self.session, model_file)

class TfCellBot(TfBot):

  def __init__(self, *args, **kwargs):
    TfBot.__init__(self, *args, **kwargs)

  def next_moves(self, stochastic=False):
    model_input, elements = self.input_provider.next_input()

    choices = self.model.predict(
        self.session, model_input, stochastic=stochastic, batch_size=self.batch_size)
    moves = [Move(self.game_map.contents[h][w], (m+4)%5)
        for ((h,w), m) in zip(elements, choices)]

    return moves

def bot_previous_frame_transform(keys, prevs, data):
  def get_prevs(game, name):
    prev_game = None
    if prevs[name] is None:
      prev_game = game
    else:
      prev_game = prevs[name]
    prevs[name] = game
    return prev_game

  for name in keys:
      data['prev_' + name] = get_prevs(data[name], name)
  return data

class MultiCellDeepInputProvider:

    def __init__(self, my_id, game_map, padding):
        self.my_id = my_id
        self.game_map = game_map
        self.padding = padding
        self.prevs = {'strengths': None, 'me': None, 'enemy': None}

    def next_input(self):
      visibility_range = self.padding
      self.game_map.get_frame()

      contents = self.game_map.contents

      prods = np.array([[[[s.production for s in row] for row in contents]]])
      strengths = np.array([[[[s.strength for s in row] for row in contents]]])
      me = np.array([[[[1 if s.owner == self.my_id else 0 for s in row] for row in contents]]])
      enemy = np.array([[[[1 if s.owner != self.my_id and s.owner != 0 else 0  for s in row] for row in contents]]])

      data = { 'productions': prods
             , 'strengths': strengths
             , 'me': me
             , 'enemy': enemy }

      transformation = chain_functions(
              partial(rotate_once_transform, 0),
              partial(normalize_transform, {'productions': 20, 'strengths': 255}),
              partial(bot_previous_frame_transform, ['strengths', 'me', 'enemy'], self.prevs),
              partial(unwrap_tranform,
                ['productions', 'strengths', 'me', 'enemy',
                'prev_strengths', 'prev_me', 'prev_enemy'],
                50, 50, visibility_range),
              partial(merge_transform, ['productions', 'strengths', 'me', 'enemy',
                'prev_strengths', 'prev_me', 'prev_enemy']))

      return transformation(data)['data']

class TfMultiCellBot(TfBot):

  def __init__(self, *args, **kwargs):
    TfBot.__init__(self, *args, **kwargs)

  def next_moves(self, stochastic=False):
    model_input = self.input_provider.next_input()
    h = self.game_map.height
    w = self.game_map.width

    choices = self.model.predict(
        self.session, model_input, stochastic=stochastic, batch_size=self.batch_size)

    h_left = 50 - h
    w_left = 50 - w
    h_u = 9 + h_left // 2
    w_l = 9 + w_left // 2
    h_u_c = h_u - 9
    w_l_c = w_l - 9
    real_choices = choices[0][h_u_c:h_u_c + h, w_l_c:w_l_c + w]
    real_input = model_input[0][h_u:h_u + h, w_l:w_l + w, 2]

    indices = np.where(real_input == 1)
    other_input = model_input[0][h_u:h_u + h, w_l:w_l + w]
    np.set_printoptions(threshold=np.nan)

    moves = [Move(self.game_map.contents[i][j], (real_choices[i][j]+4)%5)
        for (i,j) in zip(*indices)]

    return moves
