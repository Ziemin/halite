import tensorflow as tf
import logging

from library.nets import NNModel, FullLayer, Conv2dLayer, leakyReLU

def model_conv_1():
  graph = tf.Graph()
  with graph.as_default():
    X =  tf.placeholder(tf.float32, [None, 19, 19, 4], name="X")

    conv1 = Conv2dLayer('conv_1', X, [5, 5, 4, 8])
    conv2 = Conv2dLayer('conv_2', conv1.output, [5, 5, 8, 16])

    shape = conv2.output.get_shape().as_list()
    reshaped = tf.reshape(conv2.output, (-1, shape[1]*shape[2]*shape[3]))
    full1 = FullLayer('nn_1', reshaped, 256, act=tf.nn.relu)
    out = FullLayer('out', full1.output, 5)

    prediction = tf.argmax(out.output, 1)
    samples = tf.reshape(
        tf.multinomial(out.output, 1),
        [-1])
    logging.debug(prediction.get_shape().as_list())
    logging.debug(samples.get_shape().as_list())

    return NNModel(
        'ConvModel_1',
        graph, X, prediction, samples,
        [conv1, conv2, full1, out])

def model_conv_2():
  graph = tf.Graph()
  with graph.as_default():
    X =  tf.placeholder(tf.float32, [None, 19, 19, 3], name="X")

    conv1 = Conv2dLayer('conv_1', X, [5, 5, 3, 8], act=tf.nn.softplus)
    conv2 = Conv2dLayer('conv_2', conv1.output, [5, 5, 8, 16], act=tf.nn.softplus)

    shape = conv2.output.get_shape().as_list()
    reshaped = tf.reshape(conv2.output, (-1, shape[1]*shape[2]*shape[3]))
    full1 = FullLayer('nn_1', reshaped, 290, act=tf.nn.softplus)
    drop = tf.nn.dropout(full1.output, 0.9)
    out = FullLayer('out', drop, 5)

    prediction = tf.argmax(out.output, 1)

    return NNModel(
        'ConvModel_2',
        graph, X, prediction,
        [conv1, conv2, full1, drop, out])

def model_hybrid_1():
  graph = tf.Graph()
  with graph.as_default():
    X_flat =  tf.placeholder(tf.float32, [None, 19, 19, 4], name="X_flat")
    X_grid =  tf.placeholder(tf.float32, [None, 68, 68, 4], name="X_grid")

    conv1 = Conv2dLayer('conv_1', X_flat, [3, 3, 4, 100], act=leakyReLU)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 100, 128], act=leakyReLU)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 128, 80], act=leakyReLU)
    conv4 = Conv2dLayer('conv_4', conv3.output, [7, 7, 80, 40], act=leakyReLU)
    conv5 = Conv2dLayer('conv_5', conv4.output, [7, 7, 40, 5], act=None)

    m_conv1 = Conv2dLayer('m_conv_1', X_grid, act=leakyReLU,
        weights=conv1.weights, biases=conv1.biases)
    m_conv2 = Conv2dLayer('m_conv_2', m_conv1.output, act=leakyReLU,
        weights=conv2.weights, biases=conv2.biases)
    m_conv3 = Conv2dLayer('m_conv_3', m_conv2.output, act=leakyReLU,
        weights=conv3.weights, biases=conv3.biases)
    m_conv4 = Conv2dLayer('m_conv_4', m_conv3.output,
        weights=conv4.weights, biases=conv4.biases)
    m_conv5 = Conv2dLayer('m_conv_5', m_conv4.output,
        weights=conv5.weights, biases=conv5.biases)

    out = tf.reshape(conv4.output, [-1, 5])

    prediction_flat = tf.argmax(out, 1)
    samples_flat = tf.reshape(
        tf.multinomial(out, 1),
        [-1])

    prediction_grid = tf.argmax(m_conv5.output, 3)
    flat_output = tf.reshape(m_conv5.output, [-1, 5])
    samples_grid = tf.reshape(
        tf.multinomial(flat_output, 1),
        [-1, 50, 50])

    model_flat =  NNModel(
        'HybridModelFlat_1',
        graph, X_flat, prediction_flat, samples_flat,
        [conv1, conv2, conv3, conv4, conv5])

    model_grid = NNModel(
        'HybridModelGrid_1',
        graph, X_grid, prediction_grid, samples_grid,
        [m_conv1, m_conv2, m_conv3, m_conv4, m_conv5])

    return model_flat, model_grid


# This is similar to model_conv_1, but implemented to run on all the cells
# at the same time using convolutions.
# In addition statistics of averages were added to the input to
# capture information from the entire map.
def model_multi_conv_1():
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    # every cell has a view range being a rectangle [2*v+1, 2*v+1]
    # depth is 7:
    # [production, strenghts, owners, avg_prod,
    # avg_my_strength, avg_enemy_strength, avg_free_strength]
    # The last 4 channels are from avg pooling from the image of size [3w, 3w] to
    # get the information about the entire map
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [5, 5, 4, 8])
    conv2 = Conv2dLayer('conv_2', conv1.output, [5, 5, 8, 16])
    conv3 = Conv2dLayer('conv_3', conv2.output, [11, 11, 16, 256])
    conv4 = Conv2dLayer('conv_4', conv3.output, [1, 1, 256, 5], act=None)

    prediction = tf.argmax(conv4.output, 3)
    flat_output = tf.reshape(conv4.output, [-1, 5])
    samples = tf.reshape(
        tf.multinomial(flat_output, 1),
        [-1, 50, 50])

    return NNModel(
        'MultiConvModel_1',
        graph, X, prediction, samples,
        [conv1, conv2, conv3, conv4])

def get_multi_conv_model(graph, X, layers, name):
  prediction = tf.argmax(layers[-1].output, 3)
  flat_output = tf.reshape(layers[-1].output, [-1, 5])
  samples = tf.reshape(
      tf.multinomial(flat_output, 1),
      [-1, 50, 50])

  return NNModel(
      name,
      graph, X, prediction, samples,
      layers)

def model_multi_conv_2(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 100], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 100, 128], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 128, 64], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [7, 7, 64, 32], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [7, 7, 32, 5], use_batch_norm=False,
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5]

    return get_multi_conv_model(graph, X, layers, 'MultiConv_2')

def model_multi_conv_3(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 164],
        act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 164, 164],
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 164, 100],
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 100, 50],
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [11, 11, 50, 5],
      act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5]
    return get_multi_conv_model(graph, X, layers, 'MultiConv_3')

def model_multi_conv_4(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 16], act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 16, 32], act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 32, 64], act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 64, 128], act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [3, 3, 128, 64], act=leakyReLU, is_training=is_training)
    conv6 = Conv2dLayer('conv_6', conv5.output, [9, 9, 64, 5], act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, 'MultiConv_4')

def model_multi_conv_5(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 100], act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 100, 128],
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 128, 80],
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 80, 64],
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [5, 5, 64, 32],
        act=leakyReLU, is_training=is_training)
    conv6 = Conv2dLayer('conv_6', conv5.output, [7, 7, 32, 5],
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, 'MultiConv_5')

def model_multi_conv_6(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 64], act=leakyReLU,
        is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 64, 32],
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 32, 64],
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 64, 128],
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [3, 3, 128, 64],
        act=leakyReLU, is_training=is_training, dropout=0.75)
    conv6 = Conv2dLayer('conv_6', conv5.output, [9, 9, 64, 5],
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, 'MultiConv_6')

def model_multi_conv_7(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 32],
        act=tf.nn.relu, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 32, 32],
        act=tf.nn.relu, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 32, 64],
        act=tf.nn.relu, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 64, 128],
        act=tf.nn.relu, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [3, 3, 128, 64],
        act=tf.nn.relu, is_training=is_training, dropout=0.75)
    conv6 = Conv2dLayer('conv_6', conv5.output, [9, 9, 64, 5],
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, 'MultiConv_7')

def model_multi_conv_8(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 42], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 42, 42], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 42, 84], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 84, 148], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [3, 3, 148, 64], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv6 = Conv2dLayer('conv_6', conv5.output, [9, 9, 64, 5], use_batch_norm=False,
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, 'BestMultiConv_8')

def model_multi_conv_9(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 50], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 50, 50], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 50, 86], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 86, 150], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [3, 3, 150, 64], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv6 = Conv2dLayer('conv_6', conv5.output, [9, 9, 64, 5], use_batch_norm=False,
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, '6LayerMConv_9')

def model_multi_conv_10(is_training=False):
  v = 9
  w = h = 50
  graph = tf.Graph()
  with graph.as_default():
    X = tf.placeholder(tf.float32, [None, w + 2*v, w + 2*v, 4], name='X')
    conv1 = Conv2dLayer('conv_1', X, [3, 3, 4, 52], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv2 = Conv2dLayer('conv_2', conv1.output, [3, 3, 52, 52], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv3 = Conv2dLayer('conv_3', conv2.output, [3, 3, 52, 82], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv4 = Conv2dLayer('conv_4', conv3.output, [3, 3, 82, 162], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv5 = Conv2dLayer('conv_5', conv4.output, [3, 3, 162, 64], use_batch_norm=False,
        act=leakyReLU, is_training=is_training)
    conv6 = Conv2dLayer('conv_6', conv5.output, [9, 9, 64, 5], use_batch_norm=False,
        act=None, is_training=is_training)
    layers = [conv1, conv2, conv3, conv4, conv5, conv6]

    return get_multi_conv_model(graph, X, layers, '6LayerMConv_10')

def model_lstm(is_training=False):
  pass
