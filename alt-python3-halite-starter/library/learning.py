import tensorflow as tf
import logging
import os
import numpy as np
from tqdm import tqdm

import library.models as models

def cross_entropy_method_step(
    model, model_files, new_count, model_dir, name_prefix, sigma):
  session = tf.Session(graph=model.graph)
  with model.graph.as_default():
    with tf.Session(graph=model.graph) as sess:

      weights = [[] for i in range(len(model.layers))]
      biases = [[] for i in range(len(model.layers))]

      w_shapes = []
      b_shapes = []
      w_stds = []
      b_stds = []
      w_means = []
      b_means = []

      logging.info("Reading models...")
      for model_file in tqdm(model_files):
        model.restore(sess, model_file)
        for num, layer in enumerate(model.layers):
          w = sess.run(layer.weights)
          b = sess.run(layer.biases)
          w_shapes.append(w.shape)
          b_shapes.append(b.shape)

          weights[num].append(w.reshape(-1))
          biases[num].append(b.reshape(-1))

      logging.info("Computing means and diagonal covariances...")
      for ws, bs in tqdm(zip(weights, biases)):
        w_means.append(np.mean(ws, axis=0))
        b_means.append(np.mean(bs, axis=0))

        if len(model_files) == 1:
          w_stds.append(np.repeat(sigma, len(ws[0])))
          b_stds.append(np.repeat(sigma, len(bs[0])))
        else:
          w_stds.append(np.std(ws, axis=0))
          b_stds.append(np.std(bs, axis=0))

      logging.info("Generating new models...")
      new_files = []
      for i in tqdm(range(new_count)):
        model_file = os.path.join(
            model_dir, "{0}_{1}".format(name_prefix, i+1))
        new_files.append(model_file)

        new_weights = []
        new_biases = []
        for num, (ms, vs) in enumerate(zip(w_means, w_stds)):
          w = np.array([np.random.normal(mean, stdev) for mean, stdev in zip(ms, vs)])
          new_weights.append(w.reshape(w_shapes[num]))
        for num, (ms, vs) in enumerate(zip(b_means, b_stds)):
          b = np.array([np.random.normal(mean, stdev) for mean, stdev in zip(ms, vs)])
          new_biases.append(b.reshape(b_shapes[num]))

        for num, (w, b) in enumerate(zip(new_weights, new_biases)):
          op_w = model.layers[num].weights.assign(w)
          op_b = model.layers[num].biases.assign(b)
          sess.run([op_w, op_b])

        model.save(sess, model_file, write_meta_graph=False)

  session.close()
  del session
  return new_files


