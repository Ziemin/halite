import os
import sys
import argparse
import requests
import pickle
import gc
import logging
from requests.adapters import HTTPAdapter
from tqdm import tqdm

from library.data import user_games_to_df

REQUEST_TIMEOUT = 120
MAX_RETRIES = 10

def get_session():
    ses = requests.Session()
    adapter = HTTPAdapter(max_retries=MAX_RETRIES)
    ses.mount("https://", adapter)
    ses.mount("http://", adapter)
    return ses

http_session = get_session()

def get_user(username):
    logging.info("Downloading user data for {}".format('username'))
    request_url = "https://halite.io/api/web/user?username={}".format(username)
    response = requests.get(request_url)
    if not response.ok:
        logging.error("Could not download user {} due to {}".format(
            username, response.reason))
        return None
    return response.json()

def download_user_replays(user):
    '''Returns a list of pairs (replay_name, replay_json).'''

    logging.info("Downloading replays foor user {} - {}".format(
        user['username'], user['userID']))
    replays = []
    replay_prefix_url = "https://s3.amazonaws.com/halitereplaybucket/"
    user_games_url = "https://halite.io/api/web/game"
    games_data = { 'userID': user['userID'], 'limit': user['numGames'] }

    user_games_response = requests.get(user_games_url, games_data)
    if not user_games_response.ok:
        raise Exception(str(user_games_response.request))
    games = user_games_response.json()

    replays = []
    for game in tqdm(games, desc=user['username']):
        response = http_session.get(replay_prefix_url + game['replayName'])
        replays.append((game['replayName'], response.json()))
        response.close()

    response = None
    gc.collect()
    return replays

def get_top_users(number):
    logging.info("Downloading a list of top {} players".format(top))
    ranking_url = "https://halite.io/api/web/user"
    request_data = {
            'fields[]': 'isRunning',
            'limit': top,
            'orderBy': 'rank',
            'page': 0,
            'values[]': 1
            }
    response = requests.get(ranking_url, request_data)
    if not response.ok:
        logging.error("Could not download ranking due to {}".format(response.reason))
        return None
    return respones.json()
