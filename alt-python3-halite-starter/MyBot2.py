import random
import numpy as np
import os
import sys
import logging
import time
from library.bots import TfCellBot
from library.data import CellInputProvider
import library.hlt as hlt

logging.basicConfig(filename='MyBot2.log', level=logging.DEBUG)

model_file = './models/model2.ckpt'

with open(os.devnull, 'w') as sys.stderr:
  from library.models import model_conv_1
  model = model_conv_1()

  myID, game_map = hlt.get_init()

  bot = TfCellBot(model, model_file, game_map,
      CellInputProvider(myID, game_map, padding=9, on_gpu=False))

hlt.send_init("Ziemin2")

while True:
  hlt.send_frame(bot.next_moves())
